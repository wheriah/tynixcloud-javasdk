package ru.tynixcloud.sdk.request.other;

import com.google.gson.JsonObject;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import ru.tynixcloud.sdk.TynixSdk;
import ru.tynixcloud.sdk.request.BaseRequest;
import ru.tynixcloud.sdk.utility.JsonUtil;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
/**
 * Created by kaytato, for tynixcloud.
 * @VK: https://vk.com/kaytato
 */
public class OnlineRequest extends BaseRequest {


    public OnlineRequest(@NonNull TynixSdk sdk) {
        super(sdk, "online");
    }

    public OnlineResponse getGlobalOnlineResponse() throws ExecutionException, InterruptedException, IOException {
        JsonObject response = JsonUtil.parse(get(request))
                .getAsJsonObject()

                .get("response")
                .getAsJsonObject();
        int online = response.get("online").getAsInt();

        return new OnlineResponse(online);
    }

    public int getTotalOnlineResponse() throws ExecutionException, InterruptedException, IOException {
        JsonObject response = JsonUtil.parse(get(request))
                .getAsJsonObject()

                .get("response")
                .getAsJsonObject();
        int online = response.get("total").getAsInt();

        return online;
    }

    @Getter
    @FieldDefaults(level = AccessLevel.PRIVATE)
    @AllArgsConstructor
    public static class OnlineResponse {

        //int code;
        int online;


    }

}

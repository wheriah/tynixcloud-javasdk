package ru.tynixcloud.sdk.exception;
/**
 * Created by kaytato, for tynixcloud.
 * @VK: https://vk.com/kaytato
 */
public class TokenHasExpiredException extends Exception {

    @Override
    public String getLocalizedMessage() {
        return "Токен не верный, или истек его срок действия.";
    }

    @Override
    public String getMessage() {
        return "Token not found, maybe token has expired or invalid.";
    }
}

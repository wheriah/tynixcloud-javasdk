package ru.tynixcloud.sdk.exception;
/**
 * Created by kaytato, for tynixcloud.
 * @VK: https://vk.com/kaytato
 */
public class PlayerNotFoundException extends Exception {

    @Override
    public String getLocalizedMessage() {
        return "Ошибка, игрок не найден!";
    }

    @Override
    public String getMessage() {
        return "Im sorry, player not found.";
    }
}

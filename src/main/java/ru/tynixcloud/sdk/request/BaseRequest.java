package ru.tynixcloud.sdk.request;

import com.google.gson.JsonObject;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import ru.tynixcloud.sdk.TynixSdk;
import ru.tynixcloud.sdk.exception.MethodNotFoundException;
import ru.tynixcloud.sdk.exception.ParamNotFoundException;
import ru.tynixcloud.sdk.exception.TokenHasExpiredException;
import ru.tynixcloud.sdk.utility.HttpParam;
import ru.tynixcloud.sdk.utility.JsonUtil;

import java.io.IOException;
/**
 * Created by kaytato, for tynixcloud.
 * @VK: https://vk.com/kaytato
 */
@Getter
@FieldDefaults(level = AccessLevel.PROTECTED)
public abstract class BaseRequest {

    TynixSdk tynixSdk;
    String request;

    public BaseRequest(@NonNull TynixSdk sdk, @NonNull String request) {
        tynixSdk = sdk;
        this.request = request;
    }

    public String get(@NonNull String request, HttpParam... params) throws IOException {
        try {
//            JsonObject jsonObject = JsonUtil.parse(get(request))
//                    .getAsJsonObject()
//                    //.get(0)
//                    //.getAsJsonObject()
//                    ;
//            int code = jsonObject.get("code").getAsInt();
//            if (tynixSdk.isDebug()) System.out.println(String.format("[TynixSDK] :: Server has return code #%s", code));

            return tynixSdk.get(request, params);


        } catch (ParamNotFoundException | MethodNotFoundException | TokenHasExpiredException e) {
            System.out.println("[TynixSDK] :: Error " + e.getMessage());
            return null;
        }
    }


}
